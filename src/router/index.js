import Vue from 'vue'
import Router from 'vue-router'
import NProgress from 'nprogress'

import paths from './paths'
import 'nprogress/nprogress.css'

Vue.use(Router)

let router = new Router({
    mode: 'history',
    routes: paths,
})

router.beforeEach((to, from, next) => {
    NProgress.start()
    const loggedIn = localStorage.getItem('user')
    if (to.matched.some(record => record.meta.requiresAuth)) {
        if (!loggedIn) {
            next('/login')
        } else {
            next()
        }
    } else {
        next()
    }
})

router.afterEach(() => {
    NProgress.done()
})

export default router
