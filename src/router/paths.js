export default [
    {
        path: '/login',
        name: 'Login',
        component: () => import(`@/views/Login.vue`),
    },
    {
        path: '/',
        meta: { requiresAuth: true },
        component: () => import(`@/views/Thoth.vue`),
        children: [
            {
                path: '/',
                meta: { requiresAuth: true },
                name: 'Documents',
                component: () => import(`@/pages/Documents`),
            },
            {
                path: '/tag/:tag',
                meta: { requiresAuth: true },
                name: 'Tag',
                component: () => import(`@/pages/Documents`),
                props: true,
            },
            {
                path: '/:id',
                meta: { requiresAuth: true },
                name: 'Document',
                component: () => import(`@/pages/Documents`),
                props: true,
            },
        ],
    },
    { path: '*', redirect: '/' },
]
