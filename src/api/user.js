import { THOTH_CONFIG } from '@/config'

var API_URL = THOTH_CONFIG.API_URL + '/users'

export const userAPI = {
    users: () => window.axios.get(API_URL),
}
