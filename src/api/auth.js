import { THOTH_CONFIG } from '@/config'

var API_URL = THOTH_CONFIG.API_URL + '/auth'

export const authAPI = {
    login: login => window.axios.post(API_URL, login),
}
