import { THOTH_CONFIG } from '@/config'

var API_URL = THOTH_CONFIG.API_URL + '/documents'

export const documentAPI = {
    tags: () => window.axios.get(API_URL + '/tags'),
    years: () => window.axios.get( API_URL + '/years' ),
    year: year => window.axios.get( API_URL + '/year/' + year ),
    search: query => window.axios.get(API_URL + '/search/' + query),
    tag: tag => window.axios.get(API_URL + '/tag/' + tag),
    document: id => window.axios.get(API_URL + '/' + id),
    download: (document, fileIndex) =>
        window.axios.get(
            API_URL + '/' + document._id + '/download/' + fileIndex
        ),
}
