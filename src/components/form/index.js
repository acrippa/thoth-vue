import Vue from 'vue'
import checkbox from './Checkbox'
import date from './Date'
import datetime from './DateTime'
import dropdown from './Dropdown'
import number from './Number'
import text from './TextField'
import textArea from './TextArea'

const components = {}

components.install = Vue => {
    Vue.component('checkbox', checkbox)
    Vue.component('date', date)
    Vue.component('datetime', datetime)
    Vue.component('dropdown', dropdown)
    Vue.component('number', number)
    Vue.component('textfield', text)
    Vue.component('text-area', textArea)
}

Vue.use(components)
