import Vue from 'vue'
import Sidebar from './Sidebar'
import Toolbar from './Toolbar'
import Check from './filters/Check'
import Select from './filters/Select'
import './form'

const components = {}

components.install = Vue => {
    Vue.component('check', Check)
    Vue.component('selectbox', Select)
    Vue.component('sidebar', Sidebar)
    Vue.component('toolbar', Toolbar)
}

Vue.use(components)
