import _ from 'lodash'
import alert from '@/common/alert'
import { documentAPI } from '@/api'

export default {
    namespaced: true,
    state: {
        year: null,
        years: [],
        documents: [],
        document: {},
        direction: null,
        tags: [],
        filters: {
            query: '',
            direction: null,
        },
    },
    actions: {
        years: ( { commit } ) => {
            return new Promise( (resolve, reject) => {
                documentAPI.years().then(response => {
                    commit( 'years', response.data )
                    resolve()
                }, error => {
                    commit( 'years', [] )
                    reject( error )
                })
            })
        },

        year: ( { commit }, year ) => {
            commit( 'year', year )
            return new Promise( (resolve, reject) => {
                documentAPI.year( year ).then( response => {
                    commit( 'documents', response.data )
                    resolve()
                }, error => {
                    commit( 'documents', {} )
                    reject(error)
                })
            })
        },

        byTag: ({ commit }, tag) => {
            return new Promise( (resolve, reject) => {
                documentAPI.tag( tag ).then( response => {
                    commit( 'documents', response.data )
                    resolve()
                }, error => {
                    commit( 'documents', {} )
                    reject(error)
                })
            })
        },

        document: ({ commit }, id) => {
            return new Promise(resolve => {
                documentAPI.document(id).then(
                    response => {
                        commit('document', response.data)
                        resolve()
                    },
                    error => alert.error(error)
                )
            })
        },

        reset: ( { commit } ) => {
            commit( 'document', {} )
        },

        search: ({ commit }, query) => {
            documentAPI.search(query).then(
                response => {
                    commit('documents', response.data)
                },
                error => alert.error(error)
            )
        },

        tags: ({ commit }) => {
            documentAPI.tags().then(
                response => {
                    commit('tags', response.data)
                },
                error => alert.error(error)
            )
        },

        tag: ({ commit, state}, tag) => {
            let tags = state.tags
            if( _.indexOf(tags, tag)=== -1){
                tags.push(tag)
                tags.sort()
                commit('tags', tags)
            }
        },

        download: ({ state }, fileIndex = '') => {
            return documentAPI.download(state.document, fileIndex)
        },

        save: ({ commit, state }) => {
            return new Promise((resolve, reject) => {
                documentAPI.save(state.document).then(
                    response => {
                        let doc = response.data
                        let date = new Date(doc.date)
                        if (date.getFullYear() == state.year) {
                            let docs = state.documents
                            docs.splice(
                                _.findIndex(docs, element => {
                                    return element._id == doc._id
                                }),
                                1
                            )
                            docs.push(doc)
                            commit('documents', docs)
                        }
                        commit('document', doc)
                        resolve(response)
                    },
                    error => {
                        reject(error)
                    }
                )
            })
        },

        delete: ({ commit, state }) => {
            return new Promise((resolve, reject) => {
                documentAPI.delete(state.document).then(
                    response => {
                        let docs = state.documents
                        docs.splice(
                            _.findIndex(docs, element => {
                                return element._id == state.document._id
                            }),
                            1
                        )
                        commit('documents', docs)
                        resolve(response)
                    },
                    error => {
                        reject(error)
                    }
                )
            })
        },

        filter: ( { commit }, data ) => {
            commit( 'filter', data)
        },
    },

    mutations: {
        documents: (state, documents) => (state.documents = documents),
        document: (state, document) => (state.document = document),
        filter: (state, filter) => (state.filters[filter.type] = filter.value),
        years: (state, years) => (state.years = years),
        year: (state, year) => (state.year = year),
        tags: (state, tags) => (state.tags = tags),
    },
}
