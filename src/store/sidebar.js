function resizeSidebar(op) {

    if (op) {

        $(".ui.sidebar.left").addClass("very thin icon")
        $(".navslide").addClass("marginlefting")
        $(".sidebar.left span").addClass("displaynone")
        $(".sidebar .fullbar").addClass("displaynone")
        $(".ui.tinybar.displaynone").addClass("displayblock")
        $($(".logo img")[0]).addClass("displaynone")
        $($(".logo img")[1]).removeClass("displaynone")
        $(".hiddenCollapse").addClass("displaynone")

    } else {

        $(".ui.sidebar.left").removeClass("very thin icon")
        $(".navslide").removeClass("marginlefting")
        $(".sidebar.left span").removeClass("displaynone")
        $(".sidebar .fullbar").removeClass("displaynone")
        $(".ui.tinybar.displaynone").removeClass("displayblock")
        $($(".logo img")[1]).addClass("displaynone")
        $($(".logo img")[0]).removeClass("displaynone")
        $(".hiddenCollapse").removeClass("displaynone")

    }

}

export default {
    namespaced: true,
    state: {
        sideBarIsHide: false,
        manualSideBarIsHide: false,
        manualSideBarIsState: false,
    },
    mutations: {
        toggle (state) {
            state.sideBarIsHide = !state.sideBarIsHide
            state.manualSideBarIsState = !state.manualSideBarIsState
            resizeSidebar(state.manualSideBarIsState)
        }
    },
}