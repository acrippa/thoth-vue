import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex)

import app from './app'
import authentication from './authentication'
import document from './document'
import sidebar from './sidebar'
import user from './user'

export default new Vuex.Store({
    modules: {
        app,
        authentication,
        document,
        sidebar,
        user,
    },
})
