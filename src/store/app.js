export default {
    namespaced: true,
    state: {
        user: {
            locale: 'en'
        },
    },
}
