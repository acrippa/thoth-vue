import Vue from 'vue'
import Event from 'pubsub-js'
import '@/plugins'
import '@/components'
import App from '@/App'
import router from '@/router'
import store from '@/store'
import i18n from './i18n'


Vue.prototype.$event = Event
Vue.config.productionTip = false

new Vue({
    router,
    store,
    i18n,
    render: h => h(App),
}).$mount('#app')
